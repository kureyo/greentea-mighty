require('dotenv').config();

let CONFIG = {};

CONFIG.app          = process.env.APP   || 'dev';

CONFIG.server_host         = process.env.HOST  || 'localhost';
CONFIG.server_port         = process.env.PORT  || '2567';

module.exports = CONFIG;
