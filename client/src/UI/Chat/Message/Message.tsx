import React, { useState } from 'react';

interface Props {
    message: string;
    playerName: string;
}

const Message: React.FunctionComponent<Props> = (props) => {
    return (
        <div className="message">
            <p>{props.playerName}</p>
            <p>{props.message}</p>
        </div>
    );
};

export default Message;