import React, {Dispatch} from 'react';
import classNames from 'classnames';
import {ChatStatus, GlobalState} from 'greentea-mighty-shared/dist/Models';
import {MAX_PLAYER} from 'greentea-mighty-shared/dist/Constants';
import {connect} from 'react-redux';
import {Action} from 'redux';

import Message from './Message';

interface Props {
    messageList: ChatStatus[];
}

let mapStateToProps = (state: GlobalState) => {
    return {
        messageList: state.chatStatus
    };
};

const mapDispatchToProps = (dispatch: Dispatch<Action>) => {
};

const MessageBox: React.FunctionComponent<Props> = (props) => {
    return (
        <div className="messageBox">
            {
                props.messageList &&
                props.messageList.map((s) => {
                    <div className={classNames(
                        `MessageBox-${s.position}`,
                        `MessageBox--z${(s.position - 0 + MAX_PLAYER) % MAX_PLAYER}`,
                        `MessageBox---${s.isPlayer}`
                    )}>
                        <Message message={s.message} playerName={s.playerName}/>
                    </div>;
                })
            }
        </div>
    );
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)
(MessageBox);