import React from 'react';

import './InputModal.scss';

import {ChatAction} from 'greentea-mighty-shared/dist/Actions/ChatAction';
import {ChatStatus} from 'greentea-mighty-shared/dist/Models/ChatModel';

import {connect} from 'react-redux';
import {Dispatch} from 'redux';

interface Props {
    position: number,
    sessionId: string,
    onSubmit: (
        chatModel: ChatStatus,
        sessionId: string
    ) => void;
}

interface State {
    isInputVisible: boolean,
    value: string
}

const mapStateToProps = (state: void) => {

};

const mapDispatchToProps = (dispatch: Dispatch<ChatAction>) => {
};

class InputModal extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            isInputVisible: false,
            value: ""
        };

        this._handleOnKeyPress = this._handleOnKeyPress.bind(this);
        this._handleOnChange = this._handleOnChange.bind(this);

        document.addEventListener('keypress', this._handleOnKeyPress);
        console.log('InputModal Init. position :', props.position);
    }

    _handleOnChange(event: any) {
        this.setState({
            value: event.target.value
        });
    }

    _handleOnKeyPress(event: any) {
        console.log('handleKeyPress. event.key:', event.key);
        if(event.key === "Enter") {
            let message = this.state.value;

            if(this.state.isInputVisible) {
                if (message.length == 0) return;
                // do send message action
                let chatModel = {
                    position: this.props.position,
                    playerName: "test",
                    isPlayer: true,
                    message: message
                };
                this.props.onSubmit(chatModel, this.props.sessionId);
            }
            this.setState({
                isInputVisible: !this.state.isInputVisible
            });
        } else if (event.keyCode === 27 && this.state.isInputVisible) {
            this.setState({
                isInputVisible: false
            })
        }
    }

    render() {
        return (
            this.state.isInputVisible ?
                <div className="InputModal">
                    <input autoFocus type="text"
                           onChange={this._handleOnChange}
                           onKeyPress={this._handleOnKeyPress}
                           value={this.state.value}
                           tabIndex={1}/>
                </div> : <div/>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InputModal);