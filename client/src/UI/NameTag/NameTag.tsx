import React from 'react';

import './NameTag.scss';
import { CardSuit } from 'greentea-mighty-shared/dist/Constants';
import classNames from 'classnames';

interface Props {
    gameMode: string;
    playerId: number;
    name: string;
    turnOwner: boolean;
    status: 'pendency' | 'running' | 'giveup' | 'ingame';
    giruda?: CardSuit | null;
    score?: number;
    title: 'president' | 'primeMinister' | '';
}

const suitToChar = (cardSuit: CardSuit | null): string => {
    switch (cardSuit) {
        case CardSuit.Spade:
            return '♠';
        case CardSuit.Clover:
            return '♣';
        case CardSuit.Diamond:
            return '♦';
        case CardSuit.Heart:
            return '♥';
        default:
            return '❌';
    }
};

const NameTag: React.FunctionComponent<Props> = (props) => {
    return (
        <div
            className={classNames(
                'NameTag',
                `NameTag--${props.playerId}`,
                `NameTag--${props.playerId}--${props.gameMode}`,
                `NameTag--${props.status}`,
                props.turnOwner && 'NameTag--turnOwner'
            )}
        >
            <div className={classNames('NameTag-title', props.title && 'NameTag-title--' + props.title)}>
                {props.name + (props.status !== 'ingame' ? ' 후보' : '')}
            </div>
            <div className={classNames('NameTag-content')}>
                {props.status === 'pendency' && <span className="NameTag-uncertain">❔</span>}
                {props.status === 'giveup' && 'X'}
                {props.status === 'running' && (
                    <>
                        <span
                            className={classNames(
                                'NameTag-giruda',
                                props.giruda
                                    ? `NameTag-giruda--${props.giruda.toLowerCase()}`
                                    : `NameTag-giruda--nogiruda`
                            )}
                        >
                            {suitToChar(props.giruda || null)}
                        </span>
                        <span className={classNames('NameTag-score')}>
                            {props.score! - (props.giruda === null ? 1 : 0)}
                        </span>
                    </>
                )}
            </div>
        </div>
    );
};

export default NameTag;
