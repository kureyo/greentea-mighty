import React, { useState } from 'react';
import { CardSuit } from 'greentea-mighty-shared/dist/Constants';
import Modal from '../Modal';

import './PledgeInput.scss';
import classNames from 'classnames';
import { translateSuitName } from '../Common/util';

interface Props {
    playerName: string;
    minimumScore: number;
    selectedSuit: CardSuit | null;
    onClickToSubmit: (suit: CardSuit | null, score: number) => void;
    onClickToWithdraw: () => void;
}

const PledgeInput: React.FunctionComponent<Props> = (props) => {
    const [score, updateScore] = useState(props.minimumScore);
    const [suit, updateSuit] = useState(props.selectedSuit);

    if (props.minimumScore > 21) {
        // check insanity
        throw new Error('score ' + props.minimumScore + ' is unreachable');
    } else if (props.minimumScore > 20 && suit) {
        updateSuit(null);
    } else if (score < props.minimumScore) {
        // check lower bound violation
        updateScore(props.minimumScore);
    }

    const canIncrease = score < 20 || suit === null;
    const canDecrease = score > props.minimumScore;
    const canSelectNormalGiruda = score < 20;

    return (
        <Modal>
            <div className="PledgeInput">
                <p>{`[${props.playerName}] 님, 공약을 정해주십시오`}</p>
                <p className="PledgeInput-scoreBox">
                    <span>
                        {suit ? translateSuitName(suit) : '노기루다'}
                        &nbsp;
                        {suit ? score : score - 1}
                    </span>
                    <div
                        onClick={() => {
                            if (canIncrease) {
                                updateScore(score + 1);
                            }
                        }}
                        className={classNames(
                            'PledgeInput-arrow',
                            'PledgeInput-arrow--up',
                            !canIncrease && 'PledgeInput-arrow--disabled'
                        )}
                    >
                        ＋
                    </div>
                    <div
                        onClick={() => {
                            if (canDecrease) {
                                updateScore(score - 1);
                            }
                        }}
                        className={classNames(
                            'PledgeInput-arrow',
                            'PledgeInput-arrow--down',
                            !canDecrease && 'PledgeInput-arrow--disabled'
                        )}
                    >
                        −
                    </div>
                </p>
                <p className="PledgeInput-buttonStack">
                    {[
                        { suitTo: CardSuit.Spade, char: '♠' },
                        { suitTo: CardSuit.Clover, char: '♣' },
                        { suitTo: CardSuit.Diamond, char: '♦' },
                        { suitTo: CardSuit.Heart, char: '♥' },
                        { suitTo: null, char: '❌' }
                    ].map(({ suitTo, char }) => {
                        return (
                            <div
                                onClick={() => {
                                    if (canSelectNormalGiruda) {
                                        updateSuit(suitTo);
                                    }
                                }}
                                className={classNames(
                                    'PledgeInput-button',
                                    'PledgeInput-button--' +
                                        (suitTo !== null ? String(suitTo).toLowerCase() : 'noGiruda'),
                                    canSelectNormalGiruda && 'PledgeInput-button--disabled'
                                )}
                            >
                                <span>{char}</span>
                            </div>
                        );
                    })}
                </p>
                <p>
                    <span
                        className={classNames('util-button', 'util-button--primary', 'util-floatLeft')}
                        onClick={() => props.onClickToSubmit(suit, score)}
                    >
                        확인
                    </span>
                    <span
                        className={classNames('util-button', 'util-button--secondary', 'util-floatRight')}
                        onClick={props.onClickToWithdraw}
                    >
                        기권
                    </span>
                </p>
            </div>
        </Modal>
    );
};

export default PledgeInput;
