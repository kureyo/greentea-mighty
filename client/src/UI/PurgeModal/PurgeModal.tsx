import React from 'react';

import './PurgeModal.scss';
import Modal from '../Modal';
import CardSelector from '../CardSelector';
import { CardModel } from 'greentea-mighty-shared/dist/Models';
import { CardSuit } from 'greentea-mighty-shared/dist/Constants';

interface Props {
    onSelect: (card: CardModel) => void;
    filter: (card: CardModel) => boolean;
    giruda: CardSuit | null;
}

const PurgeModal: React.FunctionComponent<Props> = (props) => {
    return (
        <Modal>
            <div className="PurgeModal">
                <h1>재정비</h1>
                <p>당의 재정비가 필요합니다. 어떤 정치인은 잠시간 혹은 오랫동안 정계를 떠나게 될 수도 있습니다...</p>
                <p>잠시 '휴식'을 취할 정치인을 선택해주세요</p>
                <p>다음 카드의 소유자:</p>
                <CardSelector
                    onSelect={(card) => props.onSelect(card)}
                    cutoff={true}
                    filter={props.filter}
                    giruda={props.giruda}
                />
            </div>
        </Modal>
    );
};

export default PurgeModal;
