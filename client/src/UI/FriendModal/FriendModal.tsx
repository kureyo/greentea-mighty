import React, { useState } from 'react';

import './FriendModal.scss';
import Modal from '../Modal';
import CardSelector from '../CardSelector';
import { CardModel } from 'greentea-mighty-shared/dist/Models';
import { CardSuit } from 'greentea-mighty-shared/dist/Constants';
import classNames from 'classnames';

interface Props {
    cardInterpreter: (card: CardModel) => string;
    onSelect: (selectCard?: CardModel, selectPlayer?: number) => void;
    players: Array<{ name: string; id: number }>;
    filter: (card: CardModel) => boolean;
    giruda: CardSuit | null;
}

const FriendModal: React.FunctionComponent<Props> = (props) => {
    const [friend, setFriend] = useState<CardModel | number | undefined>(undefined);

    const friendDesc = (() => {
        if (friend === undefined) {
            return '노 프랜드';
        } else if (typeof friend === 'number') {
            const f = props.players.find((i) => i.id === friend);
            if (f) {
                return `${f.name} 프랜드`;
            } else {
                throw new Error('?? can`t find a player with id = ' + friend);
            }
        }
        return `${props.cardInterpreter(friend)} 프랜드`;
    })();

    return (
        <Modal>
            <div className="FriendModal">
                <h1>뭉치거나 혹은 홀로거나</h1>
                <div className="util-text-button" onClick={() => setFriend(undefined)}>
                    연정없이 정부 수립 (노 프랜드)
                </div>
                <p>연정 대상을 지명:</p>
                {props.players.map(({ name, id }) => (
                    <div className="util-text-button" onClick={() => setFriend(id)}>
                        {name} 프랜드
                    </div>
                ))}
                <p>다음 카드의 소유자와 연정:</p>
                <CardSelector
                    onSelect={(card) => setFriend(card)}
                    cutoff={true}
                    filter={props.filter}
                    giruda={props.giruda}
                />
                <div
                    className={classNames('util-button', 'FriendModal-confirm')}
                    onClick={() => {
                        if (friend === undefined) {
                            props.onSelect();
                        } else if (typeof friend === 'number') {
                            props.onSelect(undefined, friend);
                        } else {
                            props.onSelect(friend);
                        }
                    }}
                >
                    확인: {friendDesc}
                </div>
            </div>
        </Modal>
    );
};

export default FriendModal;
