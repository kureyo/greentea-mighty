import React, { useState } from 'react';

import { CardModel } from 'greentea-mighty-shared/dist/Models';
import Card from '../Card/index';

import classNames from 'classnames';

import './Hand.scss';
import { HAND_STATE } from '../Common/constants';
import equal from 'fast-deep-equal';

interface Props {
    state: HAND_STATE;
    count: number;
    data?: CardModel[];
    handIndex: number;
    gameMode: string;
    selectables?: boolean[];
    onSelect?: (cards: CardModel[]) => void;
}

const Hand: React.FunctionComponent<Props> = ({ state, count, data, handIndex, gameMode, selectables, onSelect }) => {
    const [selected, setSelected] = useState<CardModel[]>([]);

    const playHandler = (card: CardModel) => () => {
        onSelect && onSelect([card]);
    };

    const throwHandler = (card: CardModel) => () => {
        const idx = selected.findIndex((c) => equal(c, card));
        if (idx < 0) {
            if (selected.length < 3) {
                setSelected([...selected, card]);
            }
        } else {
            selected.splice(idx);
            setSelected([...selected]);
        }
    };

    const arr = [];
    for (let i = 0; i < count; i += 1) {
        const selectable =
            state === HAND_STATE.SELECT_THROW ? true : state === HAND_STATE.PLAY && selectables && selectables[i];
        arr.push(
            <div
                className={classNames(
                    'hand-cardWrapper',
                    data && selected.some((c) => equal(c, data[i])) && 'hand-cardWrapper--selected',
                    selectable &&
                        (state !== HAND_STATE.SELECT_THROW || selected.length < 3) &&
                        'hand-cardWrapper--selectable'
                )}
            >
                <Card
                    cardData={data && i < data.length ? data[i] : undefined}
                    selectable={selectable}
                    onClick={
                        state === HAND_STATE.SELECT_THROW
                            ? throwHandler(data![i])
                            : state === HAND_STATE.PLAY
                            ? playHandler(data![i])
                            : undefined
                    }
                />
            </div>
        );
    }

    const readyToThrow = state === HAND_STATE.SELECT_THROW && selected.length === 3;
    return (
        <>
            <div className={classNames('hand', `hand--${handIndex}`, `hand--${handIndex}-${gameMode}`)}>{arr}</div>
            {state === HAND_STATE.SELECT_THROW && (
                <div
                    className={classNames('hand-throwButton', 'util-button', !readyToThrow && 'util-button--disabled')}
                    onClick={() => readyToThrow && onSelect && onSelect(selected)}
                >
                    버린다
                </div>
            )}
        </>
    );
};

export default Hand;
