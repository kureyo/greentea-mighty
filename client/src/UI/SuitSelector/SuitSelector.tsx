import React, { useState } from 'react';
import { CardSuit } from 'greentea-mighty-shared/dist/Constants';
import './SuitSelector.scss';
import { CHAR_SPADE, CHAR_CLOVER, CHAR_DIAMOND, CHAR_NO_GIRUDA, CHAR_HEART } from '../Common/constants';
import classNames from 'classnames';

interface Props {
    showNoGiruda: boolean;
    onSelect: (suit: CardSuit | undefined) => void;
}

const SuitSelector: React.FunctionComponent<Props> = (props) => {
    const set = [
        { suitTo: CardSuit.Spade, char: CHAR_SPADE },
        { suitTo: CardSuit.Clover, char: CHAR_CLOVER },
        { suitTo: CardSuit.Diamond, char: CHAR_DIAMOND },
        { suitTo: CardSuit.Heart, char: CHAR_HEART },
        { suitTo: undefined, char: CHAR_NO_GIRUDA }
    ];
    return (
        <div className="SuitSelector">
            {set.map((d) => {
                if (!d.suitTo && !props.showNoGiruda) {
                    return null;
                }
                return (
                    <div
                        className={classNames(
                            'SuitSelector-button',
                            d.suitTo
                                ? 'SuitSelector-button--' + (d.suitTo as string).toLowerCase()
                                : 'SuitSelector-button--nogiruda'
                        )}
                        onClick={() => props.onSelect(d.suitTo)}
                    >
                        {d.char}
                    </div>
                );
            })}
        </div>
    );
};

export default SuitSelector;
