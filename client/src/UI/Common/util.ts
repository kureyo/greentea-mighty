import { CardModel, GameStatus, RoundStatus } from 'greentea-mighty-shared/dist/Models';
import CardInterpreter from 'greentea-mighty-shared/dist/GameRule/CardInterpreter';
import { CardFunction, CardRankValueAlias, CardSuit } from 'greentea-mighty-shared/dist/Constants';
import { string } from '@colyseus/schema/lib/encoding/decode';

export const translateSuitName = (suit: CardSuit): string => {
    return {
        [CardSuit.Clover]: '클로버',
        [CardSuit.Diamond]: '다이아몬드',
        [CardSuit.Heart]: '하트',
        [CardSuit.Spade]: '스페이드',
        [CardSuit.Joker]: ''
    }[suit];
};

export const textifyCard = (gameRule: GameStatus, roundRule: RoundStatus) => (card: CardModel): string => {
    const func = CardInterpreter(card, gameRule, roundRule);
    if (func === CardFunction.MIGHTY) {
        return '마이티';
    } else if (func === CardFunction.JOKER || func === CardFunction.JOKER_WEAK) {
        return '조커';
    } else if (func === CardFunction.JOKER_CALL) {
        return '조커콜';
    }
    const rankDesc = card.rank >= CardRankValueAlias.J ? 'JQKA'[card.rank - CardRankValueAlias.J] : String(card.rank);
    const suitDesc = card.suit === gameRule.giruda ? '기루다' : translateSuitName(card.suit);
    return suitDesc + ' ' + rankDesc;
};
