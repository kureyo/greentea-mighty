export const CHAR_SPADE = '♠';
export const CHAR_CLOVER = '♣';
export const CHAR_DIAMOND = '♦';
export const CHAR_HEART = '♥';
export const CHAR_NO_GIRUDA = '❌';

export enum HAND_STATE {
    IDLE,
    PLAY,
    SELECT_THROW
}
