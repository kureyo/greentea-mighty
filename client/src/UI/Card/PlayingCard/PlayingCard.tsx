// renders 'PURE' playing card
import classnames from 'classnames';
import React from 'react';

import { CardRankValueAlias, CardSuit } from 'greentea-mighty-shared/dist/Constants';
import PlayingCardInner from './PlayingCardInner';

import './PlayingCard.scss';

export interface PlayingCardProps {
    suit: CardSuit;
    rank: number;
}

const PlayingCard: React.FunctionComponent<PlayingCardProps> = (props) => {
    const character =
        props.rank < CardRankValueAlias.J ? props.rank.toString() : 'JQKA'[props.rank - CardRankValueAlias.J];

    return (
        <section
            className={classnames('playingCard', 'playingCard--' + props.suit.toLowerCase())}
            data-value={character}
        >
            <PlayingCardInner {...props} />
        </section>
    );
};

export default PlayingCard;
