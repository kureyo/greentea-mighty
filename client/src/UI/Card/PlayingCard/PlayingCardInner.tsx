import classnames from 'classnames';
import React from 'react';

import { CardRankValueAlias, CardSuit } from 'greentea-mighty-shared/dist/Constants';
import { PlayingCardProps } from './PlayingCard';
import PlayingCardColumn from './PlayingCardColumn';

const calcLines = (rank: number): number[] => {
    switch (rank) {
        case 2:
        case 3:
            return [rank, 0, 0];
        case 4:
        case 5:
        case 6:
        case 7:
            return [rank >> 1, rank % 2, rank >> 1];
        case CardRankValueAlias.A:
            return [1, 0, 0];
        default:
            const h = (rank - 1) >> 1;
            return [h, rank - 2 * h, h];
    }
};

const playingCardInner: React.FunctionComponent<PlayingCardProps> = (props) => {
    if (props.suit === CardSuit.Joker) {
        return <div className={classnames('card-pic', 'card-pic--joker')} />;
    }
    if (props.rank === CardRankValueAlias.A) {
        return (
            <div className={classnames('playingCard-inner', 'playingCard-inner--centered')}>
                <PlayingCardColumn rows={1} center={true} extraSymbolAttr={'ace'} />
            </div>
        );
    }
    if (props.rank >= CardRankValueAlias.J) {
        const symbol =
            props.rank < CardRankValueAlias.J
                ? ''
                : ['jack', 'queen', 'king', 'ace'][props.rank - CardRankValueAlias.J];
        return <div className={classnames('card-pic', 'card-pic--' + props.suit.toLowerCase() + '-' + symbol)} />;
    }
    const v = calcLines(props.rank);
    const extraSymbolAttr = props.rank === 7 ? 'huge' : v[1] === 2 ? 'big' : undefined;

    return (
        <div className={classnames('playingCard-inner', v[2] === 0 && 'playingCard-inner--centered')}>
            <PlayingCardColumn rows={v[0]} center={false} />
            <PlayingCardColumn rows={v[1]} center={true} extraSymbolAttr={extraSymbolAttr} />
            <PlayingCardColumn rows={v[2]} center={false} />
        </div>
    );
};

export default playingCardInner;
