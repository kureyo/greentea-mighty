import classnames from 'classnames';
import React from 'react';

interface Props {
    rows: number;
    center: boolean;
    extraSymbolAttr?: string;
}

const PlayingCardColumn: React.FunctionComponent<Props> = (props) => {
    if (props.rows <= 0) {
        return null;
    }

    return (
        <div className={classnames('playingCard-column', props.center && 'playingCard-column--centered')}>
            <div
                className={classnames(
                    'playingCard-symbol',
                    props.extraSymbolAttr && 'playingCard-symbol--' + props.extraSymbolAttr
                )}
            />
            {props.rows >= 2 && (
                <div
                    className={classnames(
                        'playingCard-symbol',
                        props.extraSymbolAttr && 'playingCard-symbol--' + props.extraSymbolAttr
                    )}
                />
            )}
            {props.rows >= 4 && <div className="playingCard-symbol playingCard-symbol--rotated" />}
            {props.rows >= 3 && <div className="playingCard-symbol" />}
        </div>
    );
};

export default PlayingCardColumn;
