import React from 'react';

import PlayingCard from './PlayingCard/PlayingCard';

import './Card.scss';

import { CardModel } from 'greentea-mighty-shared/dist/Models';
import classNames from 'classnames';

interface Props {
    cardData?: CardModel;
    selectable?: boolean;
    onClick?: () => void;
}

const Card: React.FunctionComponent<Props> = ({ cardData, selectable, onClick }) => {
    return (
        <div
            className={classNames(
                'card',
                selectable !== undefined && (selectable ? 'card--selectable' : 'card--unselectable')
            )}
            onClick={onClick}
        >
            {cardData && <PlayingCard suit={cardData.suit} rank={cardData.rank} />}
            {!cardData && <div className="card-pic card-pic--back" />}
        </div>
    );
};

export default Card;
