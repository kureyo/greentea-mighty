import React from 'react';
import Modal from './Modal';
import classNames from 'classnames';

interface Props {
    title: string;
    content: string;
    confirmMessage?: string;
    onClick: () => void;
}

const TemplatedModal: React.FunctionComponent<Props> = (props) => {
    return (
        <Modal>
            <h2>{props.title}</h2>
            {props.content.split('\n').map((s) => (
                <p>{s}</p>
            ))}
            <div
                className={classNames('Modal-confirmButton', 'util-button', 'util-button--primary')}
                onClick={props.onClick}
            >
                {props.confirmMessage || '확인'}
            </div>
        </Modal>
    );
};

export default TemplatedModal;
