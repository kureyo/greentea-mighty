import classnames from 'classnames';
import React, { useState } from 'react';

import Card from '../Card/index';
import { CardRankValueAlias, CardSuit } from 'greentea-mighty-shared/dist/Constants';
import _ from 'lodash';

import './CardSelector.scss';
import SuitSelector from '../SuitSelector/indext';
import { CardModel } from 'greentea-mighty-shared/dist/Models';
import classNames from 'classnames';

interface Props {
    cutoff: boolean;
    giruda: CardSuit | null;
    onSelect: (card: CardModel) => void;
    filter?: (card: CardModel) => boolean;
}

const CardSelector: React.FunctionComponent<Props> = (props) => {
    const [selectedSuit, selectSuit] = useState(CardSuit.Spade);
    const MIGHTY = {
        suit: props.giruda === CardSuit.Spade ? CardSuit.Diamond : CardSuit.Spade,
        rank: CardRankValueAlias.A
    };
    const GIRUDA_A = { suit: props.giruda!, rank: CardRankValueAlias.A };
    const GIRUDA_K = { suit: props.giruda!, rank: CardRankValueAlias.K };
    const GIRUDA_Q = { suit: props.giruda!, rank: CardRankValueAlias.Q };
    const CLOVER_A = { suit: CardSuit.Clover, rank: CardRankValueAlias.A };
    const HEART_A = { suit: CardSuit.Heart, rank: CardRankValueAlias.A };
    const DIAMOND_A = { suit: CardSuit.Diamond, rank: CardRankValueAlias.A };
    const JOKER = { suit: CardSuit.Joker, rank: 0 };

    const checkDisabled = (card: CardModel): boolean => {
        return !!props.filter && !props.filter(card);
    };
    const select = (card: CardModel) => {
        if (checkDisabled(card)) {
            return;
        }
        props.onSelect(card);
    };

    return (
        <div className="CardSelector">
            <div className="CardSelector-shortcuts">
                {props.giruda ? (
                    <>
                        <div className="CardSelector-shortcutColumn">
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(MIGHTY)}
                                onClick={() => select(MIGHTY)}
                            >
                                마이티
                            </div>
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(GIRUDA_A)}
                                onClick={() => select(GIRUDA_A)}
                            >
                                기루다A
                            </div>
                        </div>
                        <div className="CardSelector-shortcutColumn">
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(GIRUDA_K)}
                                onClick={() => select(GIRUDA_K)}
                            >
                                기루다K
                            </div>
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(GIRUDA_Q)}
                                onClick={() => select(GIRUDA_Q)}
                            >
                                기루다Q
                            </div>
                        </div>
                    </>
                ) : (
                    <>
                        <div className="CardSelector-shortcutColumn">
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(MIGHTY)}
                                onClick={() => select(MIGHTY)}
                            >
                                마이티
                            </div>
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(CLOVER_A)}
                                onClick={() => select(CLOVER_A)}
                            >
                                클로버A
                            </div>
                        </div>
                        <div className="CardSelector-shortcutColumn">
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(DIAMOND_A)}
                                onClick={() => select(DIAMOND_A)}
                            >
                                다이아A
                            </div>
                            <div
                                className="util-text-button"
                                data-disabled={checkDisabled(HEART_A)}
                                onClick={() => select(HEART_A)}
                            >
                                하트A
                            </div>
                        </div>
                    </>
                )}
            </div>
            <p>혹은</p>
            <SuitSelector showNoGiruda={false} onSelect={(suit) => suit && selectSuit(suit)} />
            <div className="util-fixedSmallSizeContainer">
                <div
                    className={classnames(
                        'CardSelector-cardsContainer',
                        props.cutoff && 'CardSelector-cardsContainer--cutoff'
                    )}
                >
                    <Card cardData={JOKER} selectable={!checkDisabled(JOKER)} onClick={() => select(JOKER)} />
                    {_.range(2, CardRankValueAlias.A + 1).map((i) => {
                        const card = { suit: selectedSuit, rank: i };
                        return (
                            <Card
                                cardData={card}
                                selectable={!checkDisabled({ suit: selectedSuit, rank: i })}
                                onClick={() => select(card)}
                            />
                        );
                    })}
                </div>
            </div>
        </div>
    );
};

export default CardSelector;
