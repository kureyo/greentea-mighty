import classnames from 'classnames';
import React from 'react';

import Card from '../Card/index';

import { CardModel } from 'greentea-mighty-shared/dist/Models';

import './CenterCards.scss';

interface Props {
    cardData: Array<CardModel | null>;
    remove: boolean;
    winner?: number;
    round: number;
    starter: number;
}

const CenterCards: React.FunctionComponent<Props> = (props) => {
    return (
        <>
            {props.cardData.map((d, idx) =>
                d ? (
                    <div
                        className={classnames(
                            'CenterCards',
                            `CenterCards-${idx}`,
                            `CenterCards--z${(idx - props.starter + 5) % 5}`,
                            props.remove && `CenterCards--removeTo${props.winner}`
                        )}
                        key={`center-cards-${props.round}-${idx}`}
                    >
                        <Card cardData={d === null ? undefined : d} />
                    </div>
                ) : null
            )}
        </>
    );
};

export default CenterCards;
