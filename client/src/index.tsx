import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import * as serviceWorker from './serviceWorker';

import { Provider } from 'react-redux';
import { createStore, Action, applyMiddleware } from 'redux';
import { GlobalState } from 'greentea-mighty-shared/dist/Models';
import GlobalReducer, { initState } from 'greentea-mighty-shared/dist/Reducers/GlobalReducer';
import PurgeChecker from 'greentea-mighty-shared/dist/Middlewares/PurgeChecker';
import { Client } from './Services/Client';
import Game from './Game';

const store = createStore<GlobalState, Action, {}, {}>(GlobalReducer, applyMiddleware(PurgeChecker));

store.dispatch({
    type: 'update'
    //payload: {"electionStatus":{"turnOwner":5,"isRunning":[true,false,false,false,false,false],"pledges":[{"giruda":"Spade","score":13},null,null,null,null,null],"minScore":14},"gameStatus":{"stage":5,"promisedEarning":16,"giruda":"Spade","roundIndex":0,"ghostId":5,"presidentId":0,"isFriendRevealed":3,"jokerAppeared":false,"players":[{"name":"Kureyo","isGhost":false,"isPresident":true,"isFriend":false,"cards":[{"suit":"Spade","rank":3},{"suit":"Clover","rank":2},{"suit":"Spade","rank":2},{"suit":"Diamond","rank":4},{"suit":"Diamond","rank":12},{"suit":"Diamond","rank":8},{"suit":"Heart","rank":7},{"suit":"Clover","rank":12},{"suit":"Spade","rank":11},{"suit":"Diamond","rank":14},{"suit":"Diamond","rank":10},{"suit":"Diamond","rank":9},{"suit":"Diamond","rank":13}],"earned":[]},{"name":"Bilma","isGhost":false,"isPresident":false,"isFriend":false,"cards":[{"suit":"Heart","rank":14},{"suit":"Clover","rank":4},{"suit":"Heart","rank":10},{"suit":"Heart","rank":13},{"suit":"Heart","rank":12},{"suit":"Clover","rank":14},{"suit":"Diamond","rank":5},{"suit":"Joker","rank":0},{"suit":"Diamond","rank":2},{"suit":"Spade","rank":5}],"earned":[]},{"name":"Gomdyoi","isGhost":false,"isPresident":false,"isFriend":false,"cards":[{"suit":"Heart","rank":5},{"suit":"Heart","rank":9},{"suit":"Heart","rank":3},{"suit":"Clover","rank":7},{"suit":"Spade","rank":13},{"suit":"Spade","rank":14},{"suit":"Diamond","rank":7},{"suit":"Spade","rank":8},{"suit":"Heart","rank":2},{"suit":"Heart","rank":8}],"earned":[]},{"name":"Hanjeok","isGhost":false,"isPresident":false,"isFriend":true,"cards":[{"suit":"Spade","rank":12},{"suit":"Diamond","rank":6},{"suit":"Spade","rank":10},{"suit":"Diamond","rank":3},{"suit":"Clover","rank":10},{"suit":"Clover","rank":9},{"suit":"Spade","rank":4},{"suit":"Spade","rank":9},{"suit":"Clover","rank":6},{"suit":"Clover","rank":5}],"earned":[]},{"name":"Aokizz","isGhost":false,"isPresident":false,"isFriend":false,"cards":[{"suit":"Heart","rank":11},{"suit":"Clover","rank":11},{"suit":"Clover","rank":8},{"suit":"Spade","rank":6},{"suit":"Clover","rank":3},{"suit":"Heart","rank":6},{"suit":"Spade","rank":7},{"suit":"Diamond","rank":11},{"suit":"Heart","rank":4},{"suit":"Clover","rank":13}],"earned":[]},{"name":"Jaehyuk","isGhost":true,"isPresident":false,"isFriend":false,"cards":[],"earned":[]}],"serverOnly":{"floorCard":[{"suit":"Clover","rank":13},{"suit":"Spade","rank":5},{"suit":"Heart","rank":8},{"suit":"Diamond","rank":10},{"suit":"Diamond","rank":9}]},"nominatedFriend":3},"roundStatus":{"starter":0,"cardsOnTable":[null,null,null,null,null,null],"jokerCalled":false,"turnOwner":0},"prevRoundStatus":{"roundWinner":-1,"cardsPlayed":[]},"eventStatus":{"events":[{"type":"ev_declarePledge","giveUp":false,"playerId":0,"pledge":{"giruda":"Spade","score":13},"index":1},{"type":"ev_declarePledge","giveUp":true,"playerId":1,"index":2},{"type":"ev_declarePledge","giveUp":true,"playerId":2,"index":3},{"type":"ev_declarePledge","giveUp":true,"playerId":3,"index":4},{"type":"ev_declarePledge","giveUp":true,"playerId":4,"index":5},{"type":"ev_declarePledge","giveUp":true,"playerId":5,"index":6},{"type":"ev_elected","playerId":0,"index":7},{"type":"ev_purge","index":8,"success":true,"targetCard":{"suit":"Diamond","rank":14},"playerId":5},{"type":"ev_openGovernment","index":9,"newPledge":{"giruda":"Spade","score":16}}],"idx":9},"events":[]}
});

const client = new Client('12345', 'mighty', store);

ReactDOM.render(
    <Provider store={store}>
        <Game comm={client} />
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
