import React, {useState, Dispatch} from 'react';

import {
    CardModel,
    ClientStatus,
    PrevRoundStatus,
    ChatStatus,
    GlobalState,
    Pledge,
    PlayerStatus
} from 'greentea-mighty-shared/dist/Models';

import CenterCards from './UI/CenterCards';
import Hand from './UI/Hand';

import './App.css';

import {
    DeclarePledgeAction,
    ElectionActions,
    PurgeRequestAction,
    ThrowCardAction,
    NominateFriendAction,
    OpeningAction
} from 'greentea-mighty-shared/dist/Actions/ElectionAction';
import {PlayCardAction} from 'greentea-mighty-shared/dist/Actions/PlayCardAction';
import PlayValidator from 'greentea-mighty-shared/dist/GameRule/PlayValidator';

import _ from 'lodash';
import {connect} from 'react-redux';
import {Action} from 'redux';
import {TemplatedModal} from './UI/Modal';
import {MAX_PLAYER, CardSuit, GameStage} from 'greentea-mighty-shared/dist/Constants';
import NameTag from './UI/NameTag';
import PledgeInput from './UI/PledgeInput';
import CardSelector from './UI/CardSelector';
import PurgeModal from './UI/PurgeModal';
import equal from 'fast-deep-equal';
import {HAND_STATE} from './UI/Common/constants';
import FriendModal from './UI/FriendModal';
import {textifyCard} from './UI/Common/util';
import InputModal from "./UI/Chat/InputModal";

interface Props {
    stage: GameStage;
    gameMode: string;
    giruda: CardSuit | null;
    absoluteTurnOwner: number;
    relativeTurnOwner: number;
    names: string[];
    cards: Array<CardModel[] | number>;
    cardsOnCenter: Array<CardModel | null>;
    cardsOnRemove?: Array<CardModel | null>;
    cardsRemoveTo: number;
    selectables?: boolean[];
    roundIndex: number;
    electionRunning: boolean[];
    electionPledges: Array<Pledge | null>;
    playerIds: number[];
    presidentId: number;
    playerStatus: PlayerStatus[];
    minimumScore: number;
    cardInterpreter: (card: CardModel) => string;
    dispatchPlayCard: (playerId: number, card: CardModel) => void;
    dispatchSubmitPledge: (playerId: number, giruda: CardSuit | null, score: number) => void;
    dispatchGiveUp: (playerId: number) => void;
    dispatchPurge: (card: CardModel) => void;
    dispatchThrow: (cards: CardModel[]) => void;
    dispatchFriend: (cardFriend?: CardModel, nominateFriend?: number) => void;
    chatList: ChatStatus[];
}

const mapStateToProps = (state: GlobalState) => {
    const mockClientStatus = {
        myPlayerId: state.gameStatus.players.findIndex((p) => !p.isGhost),
        ghost: false
    } as ClientStatus;

    console.log(state);

    const ownersCard = state.gameStatus.players[state.roundStatus.turnOwner].cards;
    const shiftArray = <T extends {} | null>(array: T[]): T[] => {
        return [...array.slice(mockClientStatus.myPlayerId), ...array.slice(0, mockClientStatus.myPlayerId)];
    };
    const ghostId = state.gameStatus.players.findIndex((p) => p.isGhost);
    const maxPlayer = MAX_PLAYER - (ghostId >= 0 ? 1 : 0);
    const isInElection = state.gameStatus.stage === GameStage.Election;
    const turnOwner = isInElection ? state.electionStatus.turnOwner : state.roundStatus.turnOwner;
    const relativeId = (id: number): number => {
        if (id < 0) {
            return id;
        }
        const ghostIn =
            ghostId >= 0 &&
            (ghostId - mockClientStatus.myPlayerId) * (id - mockClientStatus.myPlayerId) > 0 &&
            ghostId < id;
        return ((id - mockClientStatus.myPlayerId + MAX_PLAYER) % MAX_PLAYER) - (ghostIn ? 1 : 0);
    };
    const presidentId = relativeId(state.gameStatus.presidentId);

    return {
        stage: state.gameStatus.stage,
        gameMode: `${maxPlayer}ma`,
        giruda: state.gameStatus.giruda,
        absoluteTurnOwner: turnOwner,
        relativeTurnOwner: relativeId(turnOwner),
        names: shiftArray(state.gameStatus.players)
            .filter((p) => !p.isGhost)
            .map((p) => p.name),
        cards: shiftArray(state.gameStatus.players)
            .filter((p) => !p.isGhost)
            .map((p) => p.cards),
        playerIds: shiftArray(_.range(MAX_PLAYER)).filter((i) => !state.gameStatus.players[i].isGhost),
        presidentId,
        selectables:
            typeof ownersCard === 'number'
                ? undefined
                : ownersCard.map((c) =>
                    PlayValidator(state.roundStatus.turnOwner, c, {}, state.gameStatus, state.roundStatus)
                ),
        cardsOnCenter: shiftArray(_.range(MAX_PLAYER))
            .filter((i) => !state.gameStatus.players[i].isGhost)
            .map((i) => state.roundStatus.cardsOnTable[i]),
        electionRunning: shiftArray(state.electionStatus.isRunning),
        electionPledges: shiftArray(state.electionStatus.pledges),
        roundIndex: state.gameStatus.roundIndex,
        cardsRemoveTo: relativeId(state.prevRoundStatus.roundWinner),
        cardsOnRemove:
            state.prevRoundStatus.roundWinner !== -1 ? shiftArray(state.prevRoundStatus.cardsPlayed) : undefined,
        //todo : move to context
        cardInterpreter: textifyCard(state.gameStatus, state.roundStatus),
        minimumScore: state.electionStatus.minScore,
        playerStatus: shiftArray(state.gameStatus.players).filter((p) => !p.isGhost)
    };
};

const mapDispatchToProps = (dispatch: Dispatch<ElectionActions | PlayCardAction>) => {
    return {
        dispatchPlayCard: (playerId: number, card: CardModel) => {
            dispatch({
                type: 'PlayCardAction',
                playerId,
                card,
                subAction: {}
            } as PlayCardAction);
        },
        dispatchSubmitPledge: (playerId: number, giruda: CardSuit | null, score: number) => {
            dispatch({
                type: 'declarePledge',
                playerId,
                giveUp: false,
                pledge: {
                    giruda,
                    score
                }
            } as DeclarePledgeAction);
        },
        dispatchGiveUp: (playerId: number) => {
            dispatch({
                type: 'declarePledge',
                playerId,
                giveUp: true
            } as DeclarePledgeAction);
        },
        dispatchPurge: (card: CardModel) => {
            dispatch({
                type: 'purgeRequest',
                targetCard: card
            } as PurgeRequestAction);
        },
        dispatchThrow: (cards: CardModel[]) => {
            dispatch({
                type: 'throwCard',
                cards
            } as ThrowCardAction);
        },
        dispatchFriend: (cardFriend?: CardModel, nominateFriend?: number) => {
            dispatch({
                type: 'nominateFriend',
                cardFriend,
                nominateFriend
            } as NominateFriendAction);
            // tmp
            dispatch({
                type: 'open',
                newPledge: {
                    giruda: CardSuit.Spade,
                    score: 16
                }
            } as OpeningAction);
        }
    };
};

const App: React.FunctionComponent<Props> = (props) => {
    const onClickCard = (cardIdx: number) => {
        const cards = props.cards[props.relativeTurnOwner];
        if (typeof cards === 'number') {
            return;
        }
        props.dispatchPlayCard(props.absoluteTurnOwner, cards[cardIdx]);
    };
    return (
        <div className="App">
            {props.cards.map((hand, i) => {
                const handState = (() => {
                    if (props.stage === GameStage.ThrowCard && i === props.presidentId) {
                        return HAND_STATE.SELECT_THROW;
                    } else if (props.stage === GameStage.InGame && i === props.relativeTurnOwner) {
                        return HAND_STATE.PLAY;
                    }
                    return HAND_STATE.IDLE;
                })();
                return (
                    <Hand
                        state={handState}
                        key={i}
                        handIndex={i}
                        count={typeof hand === 'number' ? hand : hand.length}
                        data={typeof hand === 'number' ? undefined : hand}
                        gameMode={props.gameMode}
                        selectables={props.selectables}
                        onSelect={
                            handState === HAND_STATE.SELECT_THROW
                                ? props.dispatchThrow
                                : handState === HAND_STATE.PLAY
                                ? (cards) => props.dispatchPlayCard(props.absoluteTurnOwner, cards[0])
                                : undefined
                        }
                    />
                );
            })}
            {props.names.map((name, i) => {
                return (
                    <NameTag
                        gameMode={props.gameMode}
                        playerId={i}
                        title={
                            props.stage === GameStage.Election
                                ? ''
                                : props.playerStatus[i].isPresident
                                ? 'president'
                                : props.playerStatus[i].isFriend
                                    ? 'primeMinister'
                                    : ''
                        }
                        turnOwner={props.relativeTurnOwner === i}
                        name={name}
                        status={
                            props.stage !== GameStage.Election
                                ? 'ingame'
                                : !props.electionRunning[i]
                                ? 'giveup'
                                : props.electionPledges[i] !== null
                                    ? 'running'
                                    : 'pendency'
                        }
                        giruda={props.electionPledges[i] && (props.electionPledges[i] as Pledge).giruda}
                        score={props.electionPledges[i] ? (props.electionPledges[i] as Pledge).score : undefined}
                    />
                );
            })}
            {props.stage === GameStage.Election && (
                <PledgeInput
                    selectedSuit={
                        props.electionPledges[props.relativeTurnOwner]
                            ? props.electionPledges[props.relativeTurnOwner]!.giruda
                            : CardSuit.Spade
                    } //TODO: recommendation
                    playerName={props.names[props.relativeTurnOwner]}
                    minimumScore={props.minimumScore} //TODO
                    onClickToSubmit={(suit: CardSuit | null, score: number) => {
                        props.dispatchSubmitPledge(props.absoluteTurnOwner, suit, score);
                    }}
                    onClickToWithdraw={() => {
                        props.dispatchGiveUp(props.absoluteTurnOwner);
                    }}
                />
            )}
            <CenterCards cardData={props.cardsOnCenter} remove={false} round={0} starter={0}/>
            {props.cardsOnRemove && (
                <CenterCards
                    cardData={props.cardsOnRemove}
                    remove={true}
                    winner={props.cardsRemoveTo}
                    round={props.roundIndex}
                    starter={0}
                />
            )}
            {props.stage === GameStage.Purge && (
                <PurgeModal
                    giruda={props.giruda}
                    onSelect={(card: CardModel) => {
                        props.dispatchPurge(card);
                    }}
                    filter={(card: CardModel) => {
                        if (typeof props.cards[props.relativeTurnOwner] !== 'number') {
                            return !(props.cards[props.relativeTurnOwner] as CardModel[]).some((owned) =>
                                equal(owned, card)
                            );
                        }
                        return false;
                    }}
                />
            )}
            {props.stage === GameStage.NominateFriend && (
                <FriendModal
                    cardInterpreter={props.cardInterpreter}
                    giruda={props.giruda}
                    players={props.names.slice(1).map((name, i) => {
                        return {
                            name,
                            id: props.playerIds[i]
                        };
                    })}
                    onSelect={props.dispatchFriend}
                    filter={(card: CardModel) => {
                        if (typeof props.cards[props.relativeTurnOwner] !== 'number') {
                            return !(props.cards[props.relativeTurnOwner] as CardModel[]).some((owned) =>
                                equal(owned, card)
                            );
                        }
                        return false;
                    }}
                />
            )}
        </div>
    );
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
