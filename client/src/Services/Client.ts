import * as Colyseus from "colyseus.js";
//import CONFIG from '../../config/config';
import { Store } from 'redux';

export class Client {
    private _sessionId: string;
    private _roomName: string;
    private room?: Colyseus.Room;
    private client: Colyseus.Client;
    private store: Store;

    constructor(sId: string, roomName: string, store: Store) {
        const host = window.document.location.host.replace(/:.*/, '');
        const location = window.location;

        console.log('host :', host);
        console.log('port :', location.port)

        this._sessionId = sId;
        this._roomName = roomName;
        this.store = store;
        // this.client = new Colyseus.Client(location.protocol.replace("http", "ws") + host + (location.port ? ':'+location.port : ''));
        this.client = new Colyseus.Client('ws://localhost:2567');
        this.join(roomName);
    }

    public join(roomName: string): any {
        console.log('join ' + roomName)
        this.client.joinOrCreate(roomName).then(room => {
            this.room = room;

            let store = this.store;

            this.room.onStateChange(function(state: { stateDump:string }) {
                const payload = JSON.parse(state.stateDump);
                console.log('onStateChange received :', payload);
                store.dispatch({
                    type: 'update',
                    payload
                });
            });

            this.room.onMessage(function(message: { type: string, payload: {} }) {
                console.log('onMessage : ', message);
            });
        }).catch(e => {
            console.error("join error", e);
        });
    }

    send = (action: {}) => {
        if (this.room) {
            console.log('sending', action);
            this.room.send(action);
        } else {
            throw "no room!";
        }
    }
}
