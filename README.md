# How to test

## 사전 필요사항
- node.js 설치

## Shared library 
- shared 폴더로 이동
- npm install
- npm run build

## Client Demo
- Shared library 빌드
- client 폴더로 이동
- npm install
- npm run start

## Server Test
- TBD

# 액션 정의

## 세션 생성 

### 유저 입장 
- ActionName : UserEnter

### 유저 퇴장 
- ActionName : UserLeave

## 선거 

### 카드 받기 
- ActionName : DrawCard

### 공약 
- ActionName : Pledge

### 기권 
- ActionName : GiveUp

### 당선 
- ActionName : WinElection

### 숙청 
- ActionName : Purge

## 라운드

### 카드 내기
- ActionName : PlayCard

### 라운드 선 문양 정하기 
- ActionName : FirstSuit
- 조커 등에 사용

### 라운드 승자 결정
- ActionName : WinRound

### 친구 밝혀짐
- ActionName : FriendFound

### 조커콜
- ActionName : JokerCall

## 경기 끝

### 경기 결과
- ActionName : GameResult