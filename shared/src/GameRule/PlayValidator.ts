import { CardModel, GameStatus, RoundStatus, SubAction } from '../Models/index';
import CardInterpreter from './CardInterpreter';
import { CardFunction, CardSuit } from '../Constants/index';
import equal from 'fast-deep-equal';

const PlayValidator = (
    player: number,
    cardData: CardModel,
    subAction: SubAction,
    gameRule: GameStatus,
    roundRule: RoundStatus
): boolean => {
    if (roundRule.turnOwner !== player) {
        return false;
    }

    const role = CardInterpreter(cardData, gameRule, roundRule);

    // You can play mighty ANYTIME
    if (role === CardFunction.MIGHTY) {
        return true;
    }

    if (subAction.setLeadingSuit) {
        if (player !== roundRule.starter) {
            return false;
        }
        if (cardData.suit !== CardSuit.Joker) {
            return false;
        }
    }

    if (subAction.jokerCall) {
        if (player !== roundRule.starter) {
            return false;
        }
        if (role !== CardFunction.JOKER_CALL) {
            return false;
        }
    }

    if (typeof gameRule.players[player].cards === 'number') {
        return true;
    }

    const playerCard = gameRule.players[player].cards as CardModel[];
    if (roundRule.jokerCalled) {
        if (cardData.suit !== CardSuit.Joker) {
            if (playerCard.some((c) => c.suit === CardSuit.Joker)) {
                return false;
            }
        }
    }

    if (cardData.suit !== CardSuit.Joker && roundRule.leadingSuit) {
        if (cardData.suit !== roundRule.leadingSuit) {
            if (playerCard.some((c) => c.suit === roundRule.leadingSuit)) {
                return false;
            }
        }
    }

    return playerCard.some((c) => equal(c, cardData));
};

export default PlayValidator;
