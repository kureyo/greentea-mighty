import PlayValidator from './PlayValidator';
import { CardRankValueAlias, CardSuit } from '../Constants/index';
import { GameStatus, RoundStatus, PlayerStatus } from '../Models/index';

describe('PlayerValidator - ', () => {
    const DIAMOND_2 = {
        suit: CardSuit.Diamond,
        rank: 2
    };
    const CLOVER_3 = {
        suit: CardSuit.Clover,
        rank: 3
    };
    const CLOVER_7 = {
        suit: CardSuit.Clover,
        rank: 7
    };
    const JOKER = {
        suit: CardSuit.Joker,
        rank: 0
    };
    const MIGHTY = {
        suit: CardSuit.Spade,
        rank: CardRankValueAlias.A
    };

    it('you should play joker if you have one and the joker call card has appeared', () => {
        expect(
            PlayValidator(
                0,
                CLOVER_7,
                {},
                {
                    players: [
                        {
                            cards: [CLOVER_7, JOKER]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    turnOwner: 0,
                    jokerCalled: true
                } as RoundStatus
            )
        ).toBe(false);
    });

    it('you can play mighty even if you have a joker card and the joker call card has appeared', () => {
        expect(
            PlayValidator(
                0,
                MIGHTY,
                {},
                {
                    players: [
                        {
                            cards: [MIGHTY, JOKER]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    turnOwner: 0,
                    jokerCalled: true
                } as RoundStatus
            )
        ).toBe(true);
    });

    it('you should play a card you have', () => {
        expect(
            PlayValidator(
                0,
                CLOVER_7,
                {},
                {
                    players: [
                        {
                            cards: [MIGHTY, JOKER]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    turnOwner: 0
                } as RoundStatus
            )
        ).toBe(false);
    });

    it('don`t play a card unless it`s your turn', () => {
        expect(
            PlayValidator(
                0,
                CLOVER_7,
                {},
                {
                    players: [
                        {
                            cards: [CLOVER_7]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    turnOwner: 1
                } as RoundStatus
            )
        ).toBe(false);
    });

    it('you should follow the leading suit', () => {
        expect(
            PlayValidator(
                0,
                CLOVER_7,
                {},
                {
                    players: [
                        {
                            cards: [CLOVER_7, DIAMOND_2]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    turnOwner: 0,
                    leadingSuit: CardSuit.Diamond
                } as RoundStatus
            )
        ).toBe(false);
    });

    it('DDALMA', () => {
        expect(
            PlayValidator(
                0,
                CLOVER_7,
                {},
                {
                    players: [
                        {
                            cards: [CLOVER_7, MIGHTY]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    turnOwner: 0,
                    leadingSuit: CardSuit.Spade
                } as RoundStatus
            )
        ).toBe(false);
    });

    it('you can play mighty anytime', () => {
        expect(
            PlayValidator(
                0,
                MIGHTY,
                {},
                {
                    players: [
                        {
                            cards: [CLOVER_7, MIGHTY]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    turnOwner: 0,
                    leadingSuit: CardSuit.Clover
                } as RoundStatus
            )
        ).toBe(true);
    });

    it('you can`t call joker unless you have a joker call with being starter', () => {
        expect(
            PlayValidator(
                0,
                CLOVER_3,
                {
                    jokerCall: true
                },
                {
                    players: [
                        {
                            cards: [CLOVER_7, CLOVER_3]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    starter: 0,
                    turnOwner: 0,
                    leadingSuit: CardSuit.Heart
                } as RoundStatus
            )
        ).toBe(true);

        expect(
            PlayValidator(
                0,
                CLOVER_3,
                {
                    jokerCall: true
                },
                {
                    players: [
                        {
                            cards: [CLOVER_7, CLOVER_3]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    starter: 1,
                    turnOwner: 0,
                    leadingSuit: CardSuit.Heart
                } as RoundStatus
            )
        ).toBe(false);

        expect(
            PlayValidator(
                0,
                CLOVER_7,
                {
                    jokerCall: true
                },
                {
                    players: [
                        {
                            cards: [CLOVER_7, CLOVER_3]
                        } as PlayerStatus
                    ]
                } as GameStatus,
                {
                    starter: 1,
                    turnOwner: 0,
                    leadingSuit: CardSuit.Heart
                } as RoundStatus
            )
        ).toBe(false);
    });
});
