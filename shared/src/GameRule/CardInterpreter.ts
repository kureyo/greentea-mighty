import { CardModel } from '../Models/CardModel';
import { GameStatus, RoundStatus } from '../Models/GameModel';
import { CardFunction, CardRankValueAlias, CardSuit } from '../Constants/index';
import equal from 'fast-deep-equal';

export const DEFAULT_MIGHTY: CardModel = {
    suit: CardSuit.Spade,
    rank: CardRankValueAlias.A
};

export const SUB_MIGHTY: CardModel = {
    suit: CardSuit.Diamond,
    rank: CardRankValueAlias.A
};

export const DEFAULT_JOKER_CALL: CardModel = {
    suit: CardSuit.Clover,
    rank: 3
};

export const SUB_JOKER_CALL: CardModel = {
    suit: CardSuit.Heart,
    rank: 3
};

export const JOKER: CardModel = {
    suit: CardSuit.Joker,
    rank: 0
};

const isFirstTurnOrLastTurn = (round: number) => {
    return round === 0 || round === 9;
};

const CardInterpreter = (
    cardData: CardModel,
    gameRule: GameStatus,
    roundRule: RoundStatus
): CardFunction | undefined => {
    if (equal(cardData, JOKER)) {
        if (roundRule.jokerCalled || isFirstTurnOrLastTurn(gameRule.roundIndex)) {
            return CardFunction.JOKER_WEAK;
        }
        return CardFunction.JOKER;
    }
    if (gameRule.giruda === CardSuit.Spade && equal(cardData, SUB_MIGHTY)) {
        return CardFunction.MIGHTY;
    }
    if (gameRule.giruda !== CardSuit.Spade && equal(cardData, DEFAULT_MIGHTY)) {
        return CardFunction.MIGHTY;
    }
    if (!gameRule.jokerAppeared) {
        if (gameRule.giruda === CardSuit.Clover && equal(cardData, SUB_JOKER_CALL)) {
            return CardFunction.JOKER_CALL;
        }
        if (gameRule.giruda !== CardSuit.Clover && equal(cardData, DEFAULT_JOKER_CALL)) {
            return CardFunction.JOKER_CALL;
        }
    }
    return undefined;
};

export default CardInterpreter;
