import CardInterpreter, {
    DEFAULT_JOKER_CALL,
    DEFAULT_MIGHTY,
    JOKER,
    SUB_JOKER_CALL,
    SUB_MIGHTY
} from './CardInterpreter';
import { CardFunction, CardSuit } from '../Constants/index';
import { GameStatus, RoundStatus } from '../Models/index';

describe('CardInterpreter', () => {
    it('DEFAULT_MIGHTY is always mighty except for giruda = spade', () => {
        [CardSuit.Clover, CardSuit.Heart, CardSuit.Diamond, undefined].forEach((s) => {
            expect(CardInterpreter({ ...DEFAULT_MIGHTY }, { giruda: s } as GameStatus, {} as RoundStatus)).toBe(
                CardFunction.MIGHTY
            );
        });
        [CardSuit.Spade].forEach((s) => {
            expect(
                CardInterpreter({ ...DEFAULT_MIGHTY }, { giruda: s } as GameStatus, {} as RoundStatus)
            ).toBeUndefined();
        });
    });

    it('SUB_MIGHTY becomes mighty when giruda = spade', () => {
        [CardSuit.Clover, CardSuit.Heart, CardSuit.Diamond, undefined].forEach((s) => {
            expect(CardInterpreter({ ...SUB_MIGHTY }, { giruda: s } as GameStatus, {} as RoundStatus)).toBeUndefined();
        });
        [CardSuit.Spade].forEach((s) => {
            expect(CardInterpreter({ ...SUB_MIGHTY }, { giruda: s } as GameStatus, {} as RoundStatus)).toBe(
                CardFunction.MIGHTY
            );
        });
    });

    it('DEFAULT_JOKER_CALL is always joker call except for when giruda = clover', () => {
        [CardSuit.Spade, CardSuit.Heart, CardSuit.Diamond, undefined].forEach((s) => {
            expect(CardInterpreter({ ...DEFAULT_JOKER_CALL }, { giruda: s } as GameStatus, {} as RoundStatus)).toBe(
                CardFunction.JOKER_CALL
            );
        });
        [CardSuit.Clover].forEach((s) => {
            expect(
                CardInterpreter({ ...DEFAULT_JOKER_CALL }, { giruda: s } as GameStatus, {} as RoundStatus)
            ).toBeUndefined();
        });
    });

    it('SUB_JOKER_CALL becomes joker call when giruda = clover', () => {
        [CardSuit.Spade, CardSuit.Heart, CardSuit.Diamond, undefined].forEach((s) => {
            expect(
                CardInterpreter({ ...SUB_JOKER_CALL }, { giruda: s } as GameStatus, {} as RoundStatus)
            ).toBeUndefined();
        });
        [CardSuit.Clover].forEach((s) => {
            expect(CardInterpreter({ ...SUB_JOKER_CALL }, { giruda: s } as GameStatus, {} as RoundStatus)).toBe(
                CardFunction.JOKER_CALL
            );
        });
    });

    it('JOKER is joker except for when there`s joker call', () => {
        expect(CardInterpreter({ ...JOKER }, {} as GameStatus, { jokerCalled: false } as RoundStatus)).toBe(
            CardFunction.JOKER
        );
        expect(CardInterpreter({ ...JOKER }, {} as GameStatus, { jokerCalled: true } as RoundStatus)).toBe(
            CardFunction.JOKER_WEAK
        );
    });
});
