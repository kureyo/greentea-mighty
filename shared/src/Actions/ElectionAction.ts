import { Pledge, CardModel } from '../Models';

export interface DeclarePledgeAction {
    type: 'declarePledge';
    playerId: number;
    pledge?: Pledge;
    giveUp: boolean;
}

export interface DealMissAction {
    type: 'dealMiss';
    playerId: number;
}

export interface PurgeRequestAction {
    type: 'purgeRequest';
    targetCard: CardModel;
}

export interface PurgeResultAction {
    type: 'purgeResult';
    success: boolean;
    targetCard: CardModel;
    purgedPlayerId?: number;
    gain: CardModel[][];
}

export interface ThrowCardAction {
    type: 'throwCard';
    cards: CardModel[] | number;
}

export interface NominateFriendAction {
    type: 'nominateFriend';
    cardFriend?: CardModel;
    nominateFriend?: number;
}

export interface OpeningAction {
    type: 'open';
    newPledge: Pledge;
}

export type ElectionActions =
    | DeclarePledgeAction
    | PurgeRequestAction
    | PurgeResultAction
    | ThrowCardAction
    | NominateFriendAction
    | OpeningAction
    | DealMissAction;
