import { PlayCardAction } from './PlayCardAction';
import { UpdateAction } from './CommonAction';
import { ElectionActions } from './ElectionAction';
import { ChatAction } from "./ChatAction";

export type ALL_ACTION = PlayCardAction | UpdateAction | ElectionActions;
