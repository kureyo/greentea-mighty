import {ChatStatus} from '../Models/ChatStatus'

export interface CreateRoomAction {
    type: 'createRoom';
}

export interface JoinRoomAction {
    type: 'joinRoom';
}

export interface LeaveRoomAction {
    type: 'leaveRoom';
}

export interface SendMessageAction {
    type: 'sendMessage'
    sessionId: string
    chatModel: ChatStatus
}

export interface ReceiveMessageAction {
    type: 'receiveMessage';
    chatModel: ChatStatus;
    // 현재는 필요없음
    // roomName: string;
}

export type ChatAction = CreateRoomAction | JoinRoomAction | LeaveRoomAction | SendMessageAction | ReceiveMessageAction;