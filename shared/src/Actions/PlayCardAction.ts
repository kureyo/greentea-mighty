import { SubAction, CardModel } from '../Models';

export interface PlayCardAction {
    type: 'PlayCardAction';
    playerId: number;
    card: CardModel;
    subAction: SubAction;
}
