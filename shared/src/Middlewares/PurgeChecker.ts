import { Middleware, Dispatch } from 'redux';
import { GlobalState, CardModel } from '../Models';
import { ALL_ACTION } from '../Actions';
import _ from 'lodash';
import { MAX_PLAYER } from '../Constants';
import equal from 'fast-deep-equal';

const PurgeChecker: Middleware<{}, GlobalState> = (store) => (next) => (action: ALL_ACTION) => {
    if (action.type === 'purgeRequest') {
        const state = store.getState();
        const purgedPlayerId = _.range(MAX_PLAYER).find((i) => {
            if (state.gameStatus.players[i].isPresident) {
                return false;
            }
            if (typeof state.gameStatus.players[i].cards === 'number') {
                // actually it must not be called
                throw new Error('PurgeRequest is called in client code');
            }
            return (state.gameStatus.players[i].cards as CardModel[]).some((c) => equal(c, action.targetCard));
        });

        if (purgedPlayerId === undefined) {
            return next({
                type: 'purgeResult',
                success: false,
                targetCard: action.targetCard,
                gain: []
            });
        }

        if (state.gameStatus.serverOnly === undefined) {
            throw new Error('PurgeRequest is called in client code');
        }

        const presidentId = state.gameStatus.players.findIndex((p) => p.isPresident);
        if (presidentId < 0) {
            throw new Error("can't find a president");
        }
        const cards = _.shuffle([
            ...(state.gameStatus.players[purgedPlayerId].cards as CardModel[]),
            ...state.gameStatus.serverOnly.floorCard
        ]);
        const chunks = _.chunk(_.slice(cards, 0, 10), 2);
        chunks.splice(purgedPlayerId, 0, []);
        chunks[presidentId].push(..._.slice(cards, 10));

        return next({
            type: 'purgeResult',
            success: true,
            targetCard: action.targetCard,
            purgedPlayerId,
            gain: chunks
        });
    }
    return next(action);
};

export default PurgeChecker;
