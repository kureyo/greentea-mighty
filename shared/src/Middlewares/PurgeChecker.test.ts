import PurgeChecker from './PurgeChecker';
import { initState } from '../Reducers/GlobalReducer';
import { Middleware, MiddlewareAPI } from 'redux';
import { PurgeRequestAction } from '../Actions/ElectionAction';
import { CardModel } from '../Models';
import produce from 'immer';

describe('PurgeChecker', () => {
    it('must not fail', () => {
        const state = produce(initState, (newState) => {
            newState.gameStatus.players[1].isPresident = true;
            return newState;
        });
        const next = jest.fn();
        const action = {
            type: 'purgeRequest',
            targetCard: (initState.gameStatus.players[0].cards as CardModel[])[0]
        } as PurgeRequestAction;
        PurgeChecker({ getState: () => state } as MiddlewareAPI)(next)(action);
    });
});
