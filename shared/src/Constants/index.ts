export enum CardSuit {
    Spade = 'Spade',
    Clover = 'Clover',
    Heart = 'Heart',
    Diamond = 'Diamond',
    Joker = 'Joker'
}

export enum CardRankValueAlias {
    J = 11,
    Q = 12,
    K = 13,
    A = 14
}

export enum CardFunction {
    JOKER,
    JOKER_WEAK,
    JOKER_CALL,
    MIGHTY
}

export const MAX_PLAYER = 6;
export const MIN_SCORE = 13;

export enum GameStage {
    Election,
    Purge,
    ThrowCard,
    NominateFriend,
    Ready,
    InGame,
    Result
}
