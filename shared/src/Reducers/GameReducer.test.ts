import GameReducer, { getWinnerId, GameReducerState } from './GameReducer';
import { RoundStatus, GameStatus, CardModel } from '../Models';
import { CardSuit, CardRankValueAlias, MAX_PLAYER, GameStage } from '../Constants';
import equal from 'fast-deep-equal';
import _ from 'lodash';

const card = (suit: CardSuit, rank: number): CardModel => {
    return {
        suit,
        rank
    };
};

describe('getWinnerId', () => {
    const defaultGameStatus = {
        giruda: CardSuit.Diamond
    } as GameStatus;

    const defaultRoundStatus = {
        leadingSuit: CardSuit.Clover,
        jokerCalled: false
    } as RoundStatus;

    it('mighty wins', () => {
        expect(
            getWinnerId(
                [
                    card(CardSuit.Clover, 6),
                    card(CardSuit.Joker, 0),
                    card(CardSuit.Spade, CardRankValueAlias.A),
                    card(CardSuit.Diamond, CardRankValueAlias.A),
                    null,
                    card(CardSuit.Diamond, 9)
                ],
                defaultGameStatus,
                defaultRoundStatus
            )
        ).toBe(2);
    });

    it('joker wins', () => {
        expect(
            getWinnerId(
                [
                    card(CardSuit.Clover, 6),
                    card(CardSuit.Joker, 0),
                    card(CardSuit.Spade, CardRankValueAlias.K),
                    card(CardSuit.Diamond, CardRankValueAlias.A),
                    card(CardSuit.Diamond, 9),
                    null
                ],
                defaultGameStatus,
                defaultRoundStatus
            )
        ).toBe(1);
    });

    it('joker will los when joker call is active', () => {
        expect(
            getWinnerId(
                [
                    card(CardSuit.Clover, 6),
                    card(CardSuit.Joker, 0),
                    null,
                    card(CardSuit.Spade, CardRankValueAlias.K),
                    card(CardSuit.Diamond, CardRankValueAlias.A),
                    card(CardSuit.Diamond, 9)
                ],
                defaultGameStatus,
                {
                    ...defaultRoundStatus,
                    jokerCalled: true
                }
            )
        ).toBe(4);
    });

    it('giruda wins', () => {
        expect(
            getWinnerId(
                [
                    null,
                    card(CardSuit.Clover, 6),
                    card(CardSuit.Heart, CardRankValueAlias.A),
                    card(CardSuit.Spade, CardRankValueAlias.K),
                    card(CardSuit.Diamond, 2),
                    card(CardSuit.Diamond, 3)
                ],
                defaultGameStatus,
                defaultRoundStatus
            )
        ).toBe(5);
    });

    it('leadingSuit wins', () => {
        expect(
            getWinnerId(
                [
                    card(CardSuit.Clover, 6),
                    card(CardSuit.Heart, CardRankValueAlias.A),
                    card(CardSuit.Spade, CardRankValueAlias.K),
                    card(CardSuit.Heart, 3),
                    null,
                    card(CardSuit.Spade, 2)
                ],
                defaultGameStatus,
                defaultRoundStatus
            )
        ).toBe(0);
    });
});

describe('GameReducer', () => {
    const defaultPlayerStauts = {
        name: 'Tester',
        isFriend: false,
        isGhost: false,
        isPresident: false,
        cards: [],
        earned: []
    };
    const defaultPreviousState = {
        gameStatus: {
            stage: GameStage.InGame,
            promisedEarning: 16,
            giruda: CardSuit.Heart,
            cardFriend: card(CardSuit.Joker, 0), // Joker friend

            roundIndex: 1,
            isFriendRevealed: undefined,
            jokerAppeared: false,
            ghostId: 4,
            presidentId: 0,
            players: [
                { ...defaultPlayerStauts, cards: 9 },
                { ...defaultPlayerStauts, cards: 8 },
                { ...defaultPlayerStauts, cards: 8 },
                {
                    ...defaultPlayerStauts,
                    cards: [
                        card(CardSuit.Joker, 0),
                        card(CardSuit.Heart, 10),
                        card(CardSuit.Heart, 3),
                        card(CardSuit.Diamond, 3),
                        card(CardSuit.Clover, 9),
                        card(CardSuit.Clover, CardRankValueAlias.J),
                        card(CardSuit.Heart, 9),
                        card(CardSuit.Heart, CardRankValueAlias.A),
                        card(CardSuit.Heart, CardRankValueAlias.K)
                    ]
                },
                { ...defaultPlayerStauts, cards: 0, isGhost: true },
                { ...defaultPlayerStauts, cards: 9 }
            ]
        } as GameStatus,
        roundStatus: {
            starter: 1,
            cardsOnTable: [null, card(CardSuit.Spade, 8), card(CardSuit.Diamond, 2), null, null, null],
            leadingSuit: CardSuit.Spade,
            jokerCalled: false,
            turnOwner: 3
        } as RoundStatus,
        prevRoundStatus: {
            roundWinner: -1,
            cardsPlayed: []
        },
        eventStatus: {
            idx: 0,
            events: []
        }
    } as GameReducerState;

    it('should put the card on CardsOnTable', () => {
        const usedCard = card(CardSuit.Clover, 9);
        const nextState = GameReducer(defaultPreviousState, {
            type: 'PlayCardAction',
            playerId: 3,
            card: usedCard,
            subAction: {}
        });
        expect(nextState.roundStatus.turnOwner).toBe(5); //Ghost
        expect(nextState.roundStatus.cardsOnTable).toEqual([
            null,
            card(CardSuit.Spade, 8),
            card(CardSuit.Diamond, 2),
            usedCard,
            null,
            null
        ]);
        expect(nextState.gameStatus.players[3].cards).toHaveLength(8);
        expect((nextState.gameStatus.players[3].cards as CardModel[]).some((c) => equal(c, usedCard))).toBe(false);
    });

    it('joker friend has appeared', () => {
        const joker = card(CardSuit.Joker, 0);
        const nextState = GameReducer(defaultPreviousState, {
            type: 'PlayCardAction',
            playerId: 3,
            card: joker,
            subAction: {}
        });
        expect(nextState.roundStatus.turnOwner).toBe(5);
        expect(nextState.roundStatus.cardsOnTable).toEqual([
            null,
            card(CardSuit.Spade, 8),
            card(CardSuit.Diamond, 2),
            joker,
            null,
            null
        ]);
        expect(nextState.gameStatus.jokerAppeared).toBe(true);
        expect(nextState.gameStatus.isFriendRevealed).toBe(3);
        expect(nextState.gameStatus.players[3].cards).toHaveLength(8);
        expect(nextState.gameStatus.players[3].isFriend).toBe(true);
        expect((nextState.gameStatus.players[3].cards as CardModel[]).some((c) => equal(c, joker))).toBe(false);
    });

    const lastTurnGameReudcerStatus = {
        ...defaultPreviousState,
        roundStatus: {
            starter: 5,
            cardsOnTable: [
                card(CardSuit.Clover, 3),
                card(CardSuit.Spade, 10),
                card(CardSuit.Diamond, CardRankValueAlias.J),
                null,
                null,
                card(CardSuit.Spade, 6)
            ],
            leadingSuit: CardSuit.Spade,
            jokerCalled: false,
            turnOwner: 3
        }
    };

    it('the joker friend is dominating', () => {
        const joker = card(CardSuit.Joker, 0);
        const expectedWinner = 3;
        const nextState = GameReducer(lastTurnGameReudcerStatus, {
            type: 'PlayCardAction',
            playerId: 3,
            card: joker,
            subAction: {}
        });
        expect(nextState.gameStatus.roundIndex).toBe(2);
        expect(nextState.roundStatus.turnOwner).toBe(expectedWinner);
        expect(nextState.roundStatus.cardsOnTable).toEqual(_.fill(Array(MAX_PLAYER), null));
        expect(nextState.gameStatus.jokerAppeared).toBe(true);
        expect(nextState.gameStatus.isFriendRevealed).toBe(3);
        expect(nextState.gameStatus.players[3].cards).toHaveLength(8);
        expect((nextState.gameStatus.players[3].cards as CardModel[]).some((c) => equal(c, joker))).toBe(false);
        expect(nextState.prevRoundStatus.roundWinner).toBe(expectedWinner);
        expect(nextState.gameStatus.players[expectedWinner].earned).toEqual([
            card(CardSuit.Spade, 10),
            card(CardSuit.Diamond, CardRankValueAlias.J)
        ]);
    });

    it('... but I`m a creep, I`m a weirdo', () => {
        const joker = card(CardSuit.Joker, 0);
        const expectedWinner = 1;
        const nextState = GameReducer(
            {
                ...lastTurnGameReudcerStatus,
                roundStatus: {
                    ...lastTurnGameReudcerStatus.roundStatus,
                    jokerCalled: true
                }
            },
            { type: 'PlayCardAction', playerId: 3, card: joker, subAction: {} }
        );
        expect(nextState.gameStatus.roundIndex).toBe(2);
        expect(nextState.roundStatus.turnOwner).toBe(expectedWinner);
        expect(nextState.roundStatus.cardsOnTable).toEqual(_.fill(Array(MAX_PLAYER), null));
        expect(nextState.gameStatus.jokerAppeared).toBe(true);
        expect(nextState.gameStatus.isFriendRevealed).toBe(3);
        expect(nextState.gameStatus.players[3].cards).toHaveLength(8);
        expect((nextState.gameStatus.players[3].cards as CardModel[]).some((c) => equal(c, joker))).toBe(false);
        expect(nextState.gameStatus.players[3].earned).toHaveLength(0);
        expect(nextState.prevRoundStatus.roundWinner).toBe(expectedWinner);
    });

    it('do nothing impressive', () => {
        const usedCard = card(CardSuit.Clover, 9);
        const expectedWinner = 1;
        const nextState = GameReducer(lastTurnGameReudcerStatus, {
            type: 'PlayCardAction',
            playerId: 3,
            card: usedCard,
            subAction: {}
        });
        expect(nextState.gameStatus.roundIndex).toBe(2);
        expect(nextState.roundStatus.turnOwner).toBe(expectedWinner);
        expect(nextState.roundStatus.cardsOnTable).toEqual(_.fill(Array(MAX_PLAYER), null));
        expect(nextState.gameStatus.jokerAppeared).toBe(false);
        expect(nextState.gameStatus.isFriendRevealed).toBe(undefined);
        expect(nextState.gameStatus.players[3].cards).toHaveLength(8);
        expect((nextState.gameStatus.players[3].cards as CardModel[]).some((c) => equal(c, usedCard))).toBe(false);
        expect(nextState.prevRoundStatus.roundWinner).toBe(expectedWinner);
    });

    it('chop the round with your giruda!', () => {
        const usedCard = card(CardSuit.Heart, 3);
        const expectedWinner = 3;
        const nextState = GameReducer(lastTurnGameReudcerStatus, {
            type: 'PlayCardAction',
            playerId: 3,
            card: usedCard,
            subAction: {}
        });
        expect(nextState.gameStatus.roundIndex).toBe(2);
        expect(nextState.roundStatus.turnOwner).toBe(expectedWinner);
        expect(nextState.roundStatus.cardsOnTable).toEqual(_.fill(Array(MAX_PLAYER), null));
        expect(nextState.gameStatus.jokerAppeared).toBe(false);
        expect(nextState.gameStatus.isFriendRevealed).toBe(undefined);
        expect(nextState.gameStatus.players[3].cards).toHaveLength(8);
        expect((nextState.gameStatus.players[3].cards as CardModel[]).some((c) => equal(c, usedCard))).toBe(false);
        expect(nextState.prevRoundStatus.roundWinner).toBe(expectedWinner);
    });
});
