import { CardModel, GameStatus, RoundStatus, PrevRoundStatus, PlayerStatus, GlobalState } from '../Models';
import { PlayCardAction } from '../Actions/PlayCardAction';
import { CardSuit, CardFunction, CardRankValueAlias, MAX_PLAYER } from '../Constants';
import equal from 'fast-deep-equal';
import CardInterpreter from '../GameRule/CardInterpreter';
import produce from 'immer';
import _ from 'lodash';
import { getNewRoundSatus } from './ReducerUtils';

export const getWinnerId = (
    cardsOnTable: Array<CardModel | null>,
    gameStatus: GameStatus,
    roundStatus: RoundStatus
): number => {
    const scores = cardsOnTable.map((c) => {
        if (!c) {
            return -9999;
        }
        const f = CardInterpreter(c, gameStatus, roundStatus);
        switch (f) {
            case CardFunction.MIGHTY:
                return 9999;
            case CardFunction.JOKER:
                return 8888;
            case CardFunction.JOKER_WEAK:
                return -9999;
            default:
                const g = c.suit === gameStatus.giruda ? 1000 : 0;
                const l = c.suit === roundStatus.leadingSuit ? 100 : 0;
                return g + l + c.rank;
        }
    });
    return scores.reduce((iMax, x, i, arr) => (x > arr[iMax] ? i : iMax), 0);
};

export type GameReducerState = Pick<GlobalState, 'gameStatus' | 'roundStatus' | 'prevRoundStatus' | 'eventStatus'>;

const GameReducer = (state: GameReducerState, action: PlayCardAction): GameReducerState => {
    if (!state) {
        return state;
    }
    return produce(state, (newState) => {
        // Update round status
        // put the card cardsOnTable
        newState.roundStatus.cardsOnTable[action.playerId] = { ...action.card };
        // update leadingSuit by default case
        if (newState.roundStatus.leadingSuit === undefined) {
            if (action.card.suit !== CardSuit.Joker) {
                newState.roundStatus.leadingSuit = action.card.suit;
            }
        }
        // update subAction
        if (action.subAction.setLeadingSuit) {
            newState.roundStatus.leadingSuit = action.subAction.setLeadingSuit;
        }
        if (action.subAction.jokerCall) {
            newState.roundStatus.jokerCalled = true;
        }
        // increase turnOwner
        newState.roundStatus.turnOwner = (newState.roundStatus.turnOwner + 1) % MAX_PLAYER;
        if (newState.gameStatus.players[newState.roundStatus.turnOwner].isGhost) {
            // there's only one ghost so this should be fine
            newState.roundStatus.turnOwner = (newState.roundStatus.turnOwner + 1) % MAX_PLAYER;
        }

        // Update game status
        // check friend udpate
        if (newState.gameStatus.cardFriend && equal(newState.gameStatus.cardFriend, action.card)) {
            newState.gameStatus.players[action.playerId].isFriend = true;
            newState.gameStatus.isFriendRevealed = action.playerId;
        }
        // check joker appeared
        if (action.card.suit === CardSuit.Joker) {
            newState.gameStatus.jokerAppeared = true;
        }
        // remove the card from hand
        const oldCardOnHand = newState.gameStatus.players[action.playerId].cards;
        let newCardOnHand: CardModel[] | number;
        if (typeof oldCardOnHand === 'number') {
            newCardOnHand = oldCardOnHand - 1;
        } else {
            newCardOnHand = oldCardOnHand.filter((c) => !equal(c, action.card));
        }
        newState.gameStatus.players[action.playerId].cards = newCardOnHand;

        // Update if one round is finished
        if (newState.roundStatus.turnOwner === newState.roundStatus.starter) {
            const roundWinnerId = getWinnerId(
                newState.roundStatus.cardsOnTable,
                newState.gameStatus,
                newState.roundStatus
            );
            const newEarned = newState.roundStatus.cardsOnTable.filter((c): c is CardModel => !!c && c.rank >= 10);
            newState.gameStatus.roundIndex += 1;
            newState.gameStatus.players[roundWinnerId].earned = newState.gameStatus.players[
                roundWinnerId
            ].earned.concat(newEarned);
            newState.eventStatus.events.push({
                type: 'ev_roundFinished',
                index: newState.eventStatus.idx += 1,
                roundWinner: roundWinnerId,
                cards: [...(newState.roundStatus.cardsOnTable as CardModel[])]
            });
            newState.prevRoundStatus = {
                roundWinner: roundWinnerId,
                cardsPlayed: [...newState.roundStatus.cardsOnTable]
            };
            newState.roundStatus = getNewRoundSatus(roundWinnerId);
        }
    });
};

export default GameReducer;
