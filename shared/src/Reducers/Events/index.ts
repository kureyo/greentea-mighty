import { CardModel, Pledge } from '../../Models';

interface BaseEvent {
    type: string;
    index: number;
}

export interface DeclarePledgeEvent extends BaseEvent {
    type: 'ev_declarePledge';
    playerId: number;
    giveUp: boolean;
    pledge?: Pledge;
}

export interface DealMissEvent extends BaseEvent {
    type: 'ev_dealMiss';
    playerId?: number;
}

export interface ElectedEvent extends BaseEvent {
    type: 'ev_elected';
    playerId: number;
}

export interface PurgeEvent extends BaseEvent {
    type: 'ev_purge';
    success: boolean;
    targetCard: CardModel;
    playerId?: number;
}

export interface GainLegacyEvent extends BaseEvent {
    type: 'ev_gainLegacy';
    playerId: number;
    cards: CardModel[] | number;
}

export interface OpenGovernmentEvent extends BaseEvent {
    type: 'ev_openGovernment';
    newPledge: Pledge;
}

export interface RoundFinishedEvent extends BaseEvent {
    type: 'ev_roundFinished';
    roundWinner: number;
    cards: CardModel[];
}

export interface GameFinishedEvent extends BaseEvent {
    type: 'ev_gameFinished';
    oldPoint: number[];
    deltaPoint: number[];
    currentPoint: number[];
}

export type Events =
    | DeclarePledgeEvent
    | DealMissEvent
    | ElectedEvent
    | PurgeEvent
    | GainLegacyEvent
    | OpenGovernmentEvent
    | RoundFinishedEvent
    | GameFinishedEvent;
