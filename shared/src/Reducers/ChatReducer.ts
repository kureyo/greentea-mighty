import { GlobalState, ChatStatus } from '../Models'
import { ChatAction } from "../Actions/ChatAction";

import produce from "immer";

export type ChatReducerState = Pick<GlobalState, 'gameStatus' | 'electionStatus' | 'eventStatus' | 'roundStatus' | 'chatStatus'>;

const ChatReducer = (
    state: ChatReducerState,
    action: ChatAction
): ChatReducerState => {
    if(!state) {
        return state;
    }
    return produce(state, (newState) => {
        switch(action.type) {
            case 'receiveMessage':
                newState.chatStatus.push(
                    action.chatModel
                );
                break;
            case 'sendMessage' :
                console.log('sendMessage. state:', state);
                console.log('sendMessage. action:', state);
                newState.chatStatus.push(
                    action.chatModel
                );
                break;
        }
    })
};

export default ChatReducer;