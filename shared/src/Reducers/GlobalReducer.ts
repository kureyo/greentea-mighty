import { GlobalState, RoundStatus, PlayerStatus } from '../Models';
import ElectionReducer from './ElectionReducer';
import { ElectionActions } from '../Actions/ElectionAction';
import GameReducer from './GameReducer';
import { PlayCardAction } from '../Actions/PlayCardAction';
import { MAX_PLAYER, CardSuit, MIN_SCORE, GameStage } from '../Constants';
import _ from 'lodash';
import { getNewRoundSatus } from './ReducerUtils';
import { UpdateAction } from '../Actions/CommonAction';
import produce from 'immer';

const initCards = _.shuffle(_.range(53)).map((v) => {
    const suits = [CardSuit.Clover, CardSuit.Diamond, CardSuit.Heart, CardSuit.Spade, CardSuit.Joker];
    return {
        suit: suits[Math.floor(v / 13)],
        rank: v === 52 ? 0 : (v % 13) + 2
    };
});

export const initState = {
    electionStatus: {
        turnOwner: 0,
        isRunning: _.fill(Array(MAX_PLAYER), true),
        pledges: _.fill(Array(MAX_PLAYER), null),
        minScore: MIN_SCORE
    },
    gameStatus: {
        stage: GameStage.Election,
        promisedEarning: -1,
        giruda: null,
        roundIndex: 0,
        ghostId: -1,
        presidentId: -1,
        isFriendRevealed: undefined,
        jokerAppeared: false,
        players: [
            {
                name: 'Kureyo',
                isGhost: false,
                isPresident: false,
                isFriend: false,
                cards: initCards.slice(0, 8),
                earned: []
            } as PlayerStatus,
            {
                name: 'Bilma',
                isGhost: false,
                isPresident: false,
                isFriend: false,
                cards: initCards.slice(8, 16),
                earned: []
            } as PlayerStatus,
            {
                name: 'Gomdyoi',
                isGhost: false,
                isPresident: false,
                isFriend: false,
                cards: initCards.slice(16, 24),
                earned: []
            } as PlayerStatus,
            {
                name: 'Hanjeok',
                isGhost: false,
                isPresident: false,
                isFriend: false,
                cards: initCards.slice(24, 32),
                earned: []
            } as PlayerStatus,
            {
                name: 'Aokizz',
                isGhost: false,
                isPresident: false,
                isFriend: false,
                cards: initCards.slice(32, 40),
                earned: []
            } as PlayerStatus,
            {
                name: 'Jaehyuk',
                isGhost: false,
                isPresident: false,
                isFriend: false,
                cards: initCards.slice(40, 48),
                earned: []
            } as PlayerStatus
        ],
        serverOnly: {
            floorCard: initCards.slice(48, 53)
        }
    },
    roundStatus: getNewRoundSatus(0),
    prevRoundStatus: {
        roundWinner: -1,
        cardsPlayed: []
    },
    chatStatus: [],
    eventStatus: {
        events: [],
        idx: 0
    }
} as GlobalState;

const GlobalReducer = (state: GlobalState | undefined = initState, action: { type: string }): GlobalState => {
    const stateWithEmptyEvent = {
        ...state,
        events: []
    };
    switch (action.type) {
        case 'update':
            return {
                ...state,
                ...(action as UpdateAction).payload
            };
        case 'declarePledge':
        case 'purgeResult':
        case 'open':
        case 'throwCard':
        case 'nominateFriend':
        case 'dealMiss':
            return {
                ...stateWithEmptyEvent,
                ...ElectionReducer(stateWithEmptyEvent, action as ElectionActions)
            };
        case 'PlayCardAction':
            return {
                ...stateWithEmptyEvent,
                ...GameReducer(stateWithEmptyEvent, action as PlayCardAction)
            };
    }
    return stateWithEmptyEvent;
};

export default GlobalReducer;
