import { GlobalState, CardModel } from '../Models';
import {
    ElectionActions,
    DeclarePledgeAction,
    PurgeResultAction,
    OpeningAction,
    DealMissAction,
    ThrowCardAction,
    NominateFriendAction
} from '../Actions/ElectionAction';
import produce from 'immer';
import _ from 'lodash';
import { MAX_PLAYER, GameStage } from '../Constants';
import equal from 'fast-deep-equal';

export type ElectionReducerState = Pick<GlobalState, 'gameStatus' | 'electionStatus' | 'eventStatus' | 'roundStatus'>;

const ElectionReducer = (state: ElectionReducerState, action: ElectionActions): ElectionReducerState => {
    if (!state) {
        return state;
    }
    return produce(state, (newState) => {
        switch (action.type) {
            case 'declarePledge':
                const declare = action as DeclarePledgeAction;
                newState.eventStatus.events.push({
                    type: 'ev_declarePledge',
                    giveUp: declare.giveUp,
                    playerId: declare.playerId,
                    pledge: declare.pledge,
                    index: newState.eventStatus.idx += 1
                });

                const onPresidentElected = (playerId: number) => {
                    newState.eventStatus.events.push({
                        type: 'ev_elected',
                        playerId,
                        index: newState.eventStatus.idx += 1
                    });
                    newState.gameStatus.stage = GameStage.Purge;
                    newState.gameStatus.presidentId = playerId;
                    newState.gameStatus.players[playerId].isPresident = true;
                    newState.roundStatus.starter = playerId;
                    newState.roundStatus.turnOwner = playerId;
                };

                if (declare.giveUp) {
                    newState.electionStatus.isRunning[declare.playerId] = false;
                    newState.electionStatus.pledges[declare.playerId] = null;
                    const runners = _.range(MAX_PLAYER).filter((x) => newState.electionStatus.isRunning[x]);
                    if (runners.length === 0) {
                        newState.eventStatus.events.push({
                            type: 'ev_dealMiss',
                            index: newState.eventStatus.idx += 1
                        });
                        return;
                    } else if (runners.length === 1) {
                        const uniq = runners[0];
                        const pledge = newState.electionStatus.pledges[uniq];
                        if (pledge) {
                            onPresidentElected(uniq);
                            return;
                        }
                    }
                } else {
                    newState.electionStatus.pledges[declare.playerId] = declare.pledge!;
                    newState.electionStatus.minScore = declare.pledge!.score + 1;
                    if (declare.pledge!.score > 20) {
                        onPresidentElected(declare.playerId);
                        return;
                    }
                }
                newState.electionStatus.turnOwner = (newState.electionStatus.turnOwner + 1) % MAX_PLAYER;
                while (!newState.electionStatus.isRunning[newState.electionStatus.turnOwner]) {
                    newState.electionStatus.turnOwner = (newState.electionStatus.turnOwner + 1) % MAX_PLAYER;
                }
                break;
            case 'dealMiss':
                newState.eventStatus.events.push({
                    type: 'ev_dealMiss',
                    playerId: (action as DealMissAction).playerId,
                    index: newState.eventStatus.idx += 1
                });
                break;
            case 'purgeResult':
                const purge = action as PurgeResultAction;
                newState.eventStatus.events.push({
                    type: 'ev_purge',
                    index: newState.eventStatus.idx += 1,
                    success: purge.success,
                    targetCard: purge.targetCard,
                    playerId: purge.purgedPlayerId
                });
                if (purge.success) {
                    newState.gameStatus.stage = GameStage.ThrowCard;
                    newState.gameStatus.ghostId = purge.purgedPlayerId!;
                    const ghost = newState.gameStatus.players[purge.purgedPlayerId!];
                    ghost.isGhost = true;
                    ghost.cards = [];
                    purge.gain.forEach((gain, index) => {
                        if (typeof newState.gameStatus.players[index].cards !== 'number') {
                            newState.gameStatus.players[index].cards = [
                                ...(newState.gameStatus.players[index].cards as CardModel[]),
                                ...gain
                            ];
                        } else if (gain.length > 0) {
                            throw new Error(
                                `gain is ${gain}, but index ${index}'s cards value was ${newState.gameStatus.players[index].cards}`
                            );
                        }
                    });
                }
                break;
            case 'throwCard':
                const throwCard = action as ThrowCardAction;
                if (newState.gameStatus.presidentId < 0) {
                    throw new Error('presidentId is negative');
                }
                const president = newState.gameStatus.players[newState.gameStatus.presidentId];
                if (typeof president.cards !== typeof throwCard.cards) {
                    throw new Error(
                        `president card type ${typeof president.cards} !== throw card type ${typeof throwCard.cards}`
                    );
                }
                if (typeof president.cards === 'number') {
                    (president.cards as number) -= throwCard.cards as number;
                } else {
                    const removes = throwCard.cards as CardModel[];
                    president.cards = (president.cards as CardModel[]).filter(
                        (c) => removes.findIndex((d) => equal(c, d)) < 0
                    );
                }
                newState.gameStatus.stage = GameStage.NominateFriend;
                break;
            case 'nominateFriend':
                const nominateFriend = action as NominateFriendAction;
                newState.gameStatus.cardFriend = nominateFriend.cardFriend;
                newState.gameStatus.nominatedFriend = nominateFriend.nominateFriend;
                if (nominateFriend.nominateFriend) {
                    newState.gameStatus.players[nominateFriend.nominateFriend].isFriend = true;
                    newState.gameStatus.isFriendRevealed = nominateFriend.nominateFriend;
                }
                newState.gameStatus.stage = GameStage.Ready;
                break;
            case 'open':
                const open = action as OpeningAction;
                newState.gameStatus.stage = GameStage.InGame;
                newState.gameStatus.giruda = open.newPledge.giruda;
                newState.gameStatus.promisedEarning = open.newPledge.score;
                if (!newState.gameStatus.giruda) {
                    newState.gameStatus.promisedEarning -= 1;
                }
                newState.eventStatus.events.push({
                    type: 'ev_openGovernment',
                    index: newState.eventStatus.idx += 1,
                    newPledge: open.newPledge
                });
                newState.gameStatus.stage = GameStage.InGame;
                break;
        }
    });
};

export default ElectionReducer;
