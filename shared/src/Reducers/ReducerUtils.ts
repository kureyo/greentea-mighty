import { RoundStatus } from '../Models';
import { MAX_PLAYER } from '../Constants';
import _ from 'lodash';

export const getNewRoundSatus = (starter: number): RoundStatus => {
    return {
        starter,

        cardsOnTable: _.fill(Array(MAX_PLAYER), null),
        jokerCalled: false,
        turnOwner: starter
    };
};
