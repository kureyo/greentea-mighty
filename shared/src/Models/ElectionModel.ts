import { CardSuit } from '../Constants';

export interface Pledge {
    giruda: CardSuit | null;
    score: number;
}

export interface ElectionStatus {
    turnOwner: number;
    isRunning: boolean[];
    pledges: Array<Pledge | null>;
    minScore: number;
}
