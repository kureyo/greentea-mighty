import {GameStatus, PrevRoundStatus, RoundStatus} from './GameModel';
import {ElectionStatus} from './ElectionModel';
import {Events} from '../Reducers/Events';
import {ChatStatus} from "./ChatStatus";

export * from './CardModel';
export * from './GameModel';
export * from './ElectionModel';
export * from './ChatStatus';

export interface GlobalState {
    electionStatus: ElectionStatus;
    gameStatus: GameStatus;
    roundStatus: RoundStatus;
    prevRoundStatus: PrevRoundStatus;
    chatStatus: ChatStatus[];
    eventStatus: {
        idx: number;
        events: Events[];
    }
}
