import { CardSuit, GameStage } from '../Constants/index';
import { CardModel } from './CardModel';

export interface PlayerStatus {
    name: string;

    isGhost: boolean;
    isPresident: boolean;
    isFriend: boolean;

    cards: CardModel[] | number;
    earned: CardModel[];
}

export interface GameStatus {
    stage: GameStage;

    readonly giruda: CardSuit | null;
    readonly promisedEarning: number;

    readonly cardFriend?: CardModel;
    readonly nominatedFriend?: number;

    roundIndex: number;

    ghostId: number;
    presidentId: number;
    isFriendRevealed?: number;
    jokerAppeared: boolean;

    players: PlayerStatus[];

    serverOnly?: {
        floorCard: CardModel[];
    };
}

export interface RoundStatus {
    readonly starter: number;

    cardsOnTable: Array<CardModel | null>;
    leadingSuit?: CardSuit; // Can be empty when a joker player said that
    jokerCalled: boolean;
    turnOwner: number;
}

export interface PrevRoundStatus {
    roundWinner: number;
    cardsPlayed: Array<CardModel | null>;
}

export interface ClientStatus {
    myPlayerId: number;
    ghost: boolean;
}

export interface SubAction {
    setLeadingSuit?: CardSuit;
    jokerCall?: boolean;
}
