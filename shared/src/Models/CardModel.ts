import { CardSuit } from '../Constants/index';

export interface CardModel {
    suit: CardSuit;
    rank: number;
}
