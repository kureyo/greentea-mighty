export interface ChatStatus {
    position: number,
    playerName: string,
    message: string,
    isPlayer: boolean
}