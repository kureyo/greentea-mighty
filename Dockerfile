# prerequisites
# 1. do npm pack in shared
# 2. do npm run build in client

# steps
# 1. docker build -t kireyo/greentea-mighty .
# 2. docker run -p 8000:2567 -d kireyo/greentea-mighty
# 3. open localhost:8000

# cheat sheets
# docker run -it kireyo/greentea-mighty /bin/bash
# docker rm $(docker ps -a -q)
# docker stop $(docker ps -a -q)
# docker rmi $(docker images -q)

FROM node:10
EXPOSE 2567

WORKDIR /usr/src/app
RUN mkdir -p /usr/src/app/shared
RUN mkdir -p /usr/src/app/server
RUN mkdir -p /usr/src/app/server/static
RUN mkdir -p /usr/src/app/client

COPY shared/greentea-mighty-shared-1.0.0.tgz ./shared/
COPY server/package.json ./server/

COPY shared/ ./shared/
COPY client/ ./client/

#WORKDIR /usr/src/app/shared
#RUN npm install greentea-mighty-shared-1.0.0.tgz

#WORKDIR /usr/src/app/client
#RUN npm install

WORKDIR /usr/src/app/server
COPY server/ .
RUN npm run init

RUN cp -r ../client/build/ ./static/

COPY client/build ./server/static

CMD ["npm", "run", "start"]