import {Client, Room} from "colyseus";
import { createStore, Action, applyMiddleware, Store } from 'redux';
import { GlobalState } from 'greentea-mighty-shared/dist/Models';
import GlobalReducer, { initState } from 'greentea-mighty-shared/dist/Reducers/GlobalReducer';
import PurgeChecker from 'greentea-mighty-shared/dist/Middlewares/PurgeChecker';
import { Schema, type } from "@colyseus/schema";
import http from 'http';

class TmpState extends Schema {
    @type("string")
    stateDump: string;
}

export class MightyRoom extends Room {
    maxClients = 6;

    private store?:Store<GlobalState>;

    onCreate (options: any) {
        this.setState(new TmpState());
        this.store = createStore<GlobalState, Action, {}, {}>(GlobalReducer, applyMiddleware(PurgeChecker));
        this.state.stateDump = JSON.stringify(this.store.getState());
    }

    onAuth(client: Client, options: any, request: http.IncomingMessage) {
        return true;
    }

    onJoin (client: Client, options: any) {
        this.send(client, {
            type: 'debug',
            payload: 'greeting!'
        });
    }

    onMessage (client: Client, message: any) {
        console.log('got message:', message);
        this.store.dispatch(message);
        this.state.stateDump = JSON.stringify(this.store.getState());
        console.log('stateDump : ', this.state.stateDump);
    }

    onLeave (client: Client, consented: boolean) {}

    onDispose() {}
}